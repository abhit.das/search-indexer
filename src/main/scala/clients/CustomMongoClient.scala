package ai.inviz
package clients

import configs.Config

import com.mongodb.{MongoClient, MongoClientOptions, MongoClientSettings, MongoClientURI, MongoCredential, ServerAddress}
object CustomMongoClient extends Config{
  System.setProperty("javax.net.ssl.keyStore", "secrets/key-store.ts");
  System.setProperty("javax.net.ssl.keyStorePassword", "test1234");
  val mongoClient : MongoClient = new MongoClient(new MongoClientURI(dbUrl))
}
