package ai.inviz
package clients

import ai.inviz.configs.Config
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.client.CredentialsProvider
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.client.{RestClient, RestClientBuilder}

object ElasticClient extends Config{

//  val credentialsProvider : CredentialsProvider = new BasicCredentialsProvider()
//  credentialsProvider.setCredentials(AuthScope.ANY,
//    new UsernamePasswordCredentials(esUserName, esPassword));

//  val esClient : RestClient = RestClient.builder(new HttpHost(esHost, esPort.toInt))
//    .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//      def customizeHttpClient(httpClientBuilder: HttpAsyncClientBuilder): HttpAsyncClientBuilder = httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)})
//    .build();

  val esClient : RestClient = RestClient.builder(new HttpHost(esHost, esPort.toInt)).build()
}
