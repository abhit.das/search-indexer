package ai.inviz
package indexer

import configs.Config

import ai.inviz.clients.CustomMongoClient.mongoClient
import ai.inviz.clients.ElasticClient
import com.mongodb.MongoClient
import org.apache.http.HttpEntity
import org.apache.http.entity.ContentType
import org.apache.http.nio.entity.NStringEntity
import org.apache.spark.sql.SparkSession
import org.codehaus.jackson.map.ObjectMapper
import org.elasticsearch.client.{Response, RestClient}
import org.mongodb.scala.Document
import org.mongodb.scala.model.{CreateCollectionOptions, Filters, Projections, Sorts, ValidationOptions}
import play.api.libs.json.Json

import java.util
import java.util.Date
import scala.collection.convert.ImplicitConversions.`iterable AsScalaIterable`
import scala.util.control.Breaks.{break, breakable}

class ESIndexer(partition : Int) extends Config{
  println("Spark App Started ...")

  @transient
  val spark =
    if (env.equalsIgnoreCase("PROD")) SparkSession.builder.config(conf).getOrCreate()
    else SparkSession.builder.config(conf).config("spark.master", "local[*]").getOrCreate()

  @transient
  val sc = spark.sparkContext

  var lastUpdateAt = findLastUpdated(mongoClient, sourceDBName, sparkIndexerMetaCollectionName)
  val documentsToIndex = if(lastUpdateAt==null) loadAllData(mongoClient, sourceDBName, sourceCollectionName)
                        else loadUpdatedData(mongoClient, sourceDBName, sourceCollectionName, lastUpdateAt)

  val startedAt = new Date()//todo: PUT AN ENTRY TO TRACK ONGOING CRON
  if (lastUpdateAt==null) lastUpdateAt = new Date()

  //val docRDD = sc.makeRDD(documentsToIndex)
//  val esDocPartitions: RDD[String] = docRDD.mapPartitions { iter =>
//
//    val lis = iter.toList
//    val ite = lis.size / 10000
//    val res =  for (i <- 0 to ite) yield {
//      if(i == ite){
//        lis.slice(i*10000,lis.size).mkString("")
//      }else{
//        lis.slice(i,i*10000+10000).mkString("")
//      }
//    }
//    res.toList.toIterator
//  }
//
//  val esDocPartitions = documentsToIndex.slice(0, 1).mkString("")
//  esDocPartitions.map{ x =>
//    println("Indexing Started")
//    val esClient : RestClient  = ElasticClient.esClient
//    Thread.sleep(20)
//    println(x.toString)
//    bulkIndex(x.toString, esClient)
//  }

  var allBulkIndexBatchSuccess = true

  for(i <- 0 to documentsToIndex.size/esBulkIndexBreadCrumbSize) {
    println("Indexing Started")
    val esClient : RestClient  = ElasticClient.esClient
    Thread.sleep(20)
    val indexingStatus = bulkIndex(documentsToIndex.slice(i*esBulkIndexBreadCrumbSize, (i*esBulkIndexBreadCrumbSize)+esBulkIndexBreadCrumbSize).mkString(""), esClient)
    if (indexingStatus == 500) allBulkIndexBatchSuccess = false
  }

  if (allBulkIndexBatchSuccess) saveSchedulerMetaData(mongoClient, sourceDBName, sparkIndexerMetaCollectionName, startedAt, lastUpdateAt)
  println("Successfully Indexed and Updated SparkIndexerMeta ...")


  private def findLastUpdated(mongoClient: MongoClient, databaseName: String, collectionName: String): Date = {
    val lastUpdateRecord = mongoClient.getDatabase(databaseName).getCollection(collectionName)
      .find().sort(Sorts.descending("lastUpdatedAt"))

    if (lastUpdateRecord.first() == null){
      null;
    } else {
      lastUpdateRecord.first().getDate("lastUpdatedAt")
    }
  }

  private def loadAllData(mongoClient: MongoClient, databaseName: String, collectionName: String): List[String] = {
    val a = mongoClient.getDatabase(databaseName).getCollection(collectionName).find(Filters.equal("is_valid",true));
    a.map( doc => {
      val id = doc.getString("id")
      val indexName = doc.getString("index")

      val mapper = new ObjectMapper()
      val docContent = mapper.writeValueAsString(doc.get("payload"))

      makeESPayload(indexName, id, docContent)
    }).toList
  }

  private def loadUpdatedData(mongoClient: MongoClient, databaseName: String, collectionName: String, lastUpdated: Date): List[String] = {
    mongoClient.getDatabase(databaseName).getCollection(collectionName)
      .find().filter(Filters.eq("is_valid", true))
      .filter(Filters.gt("updated_date", lastUpdated))
      .sort(Sorts.descending("updated_date"))
      .map( doc => {
        val id = doc.getString("id")
        val indexName = doc.getString("index")
        val mapper = new ObjectMapper()
        val docContent = mapper.writeValueAsString(doc.get("payload"))

        makeESPayload(indexName, id, docContent)
    }).toList
  }

  private def saveSchedulerMetaData(mongoClient: MongoClient, databaseName: String, sparkIndexerMetaCollectionName: String, startedAt: Date, lastUpdateAt: Date) : Unit = {
    val metaDb = mongoClient.getDatabase(databaseName)
    var isCollectionExists = false;

    breakable {
      for (name <- metaDb.listCollectionNames()) {
        if (name.equalsIgnoreCase(sparkIndexerMetaCollectionName)) {
          isCollectionExists = true
          break
        }
      }
    }

    if (!isCollectionExists) {
      val collOptions : ValidationOptions = ValidationOptions().validator(
        Filters.or(Filters.exists("lastUpdatedAt"), Filters.exists("startedAt")))

      metaDb.createCollection(sparkIndexerMetaCollectionName, CreateCollectionOptions().validationOptions(collOptions))
    }

    val metaDoc: Document = Document("startedAt" -> startedAt, "lastUpdatedAt" -> lastUpdateAt)

    metaDb.getCollection(sparkIndexerMetaCollectionName).insertOne(metaDoc)
  }


  private def getJson(index: String, _id: String, indexData: String, indexType: String): String = {

    Json.obj("index" ->
      Json.obj(
        "_index" -> index,
        "_type" -> indexType,
        "routing" -> _id,
        "_id" -> _id)) + "\n" + indexData+"\n"
  }

  private def makeESPayload(indexName: String, _id: String, indexData: String): String = {
    Json.obj("index" ->
      Json.obj(
        "_index" -> indexName,
        "routing" -> _id,
        "_id" -> _id)) + "\n" + indexData+"\n"
  }

  private def bulkIndex(payload: String, restClient: RestClient) : Int ={
    println("Indexing ..........."+ payload)

    try{
      val params: util.Map[String, String] = new util.HashMap[String,String]()
      val entity: HttpEntity = new NStringEntity(payload, ContentType.APPLICATION_JSON)
      val response: Response =  restClient.performRequest("POST", "/_bulk", params, entity)
      val status :Int = response.getStatusLine.getStatusCode
      if(status == 200){
        println("Indexed successfully ...........")
      }
      else {
        println("Error Occured in Indexing ...........")
      }
      status
    }catch {
      case e : Exception =>{
        e.printStackTrace()
        500
      }
    }
  }
}
