package ai.inviz
package configs

import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkConf

trait Config extends Serializable {
  private val config = ConfigFactory.load()
  private val databaseConfig = config.getConfig("database")
  private val elasticSearchConfig = config.getConfig("elasticsearch")
  private val environmentConfig = config.getConfig("environment")

  val dbUrl = databaseConfig.getString("url")
  val sourceDBName = databaseConfig.getString("sourceDBName")
  val sourceCollectionName = databaseConfig.getString("sourceCollectionName")
  val sparkIndexerMetaCollectionName = databaseConfig.getString("sparkIndexerMetaCollectionName")
  val env = environmentConfig.getString("env")
  val esHost = elasticSearchConfig.getString("host")
  val esPort = elasticSearchConfig.getString("port")
  val esBulkIndexBreadCrumbSize = elasticSearchConfig.getInt("breadcrumbSize")
  val esUrl = s"http://$esHost:$esPort"

  val conf = new SparkConf()
    .setAppName("Search Indexer")
    .set("spark.master", "local[*]")
    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .set("spark.kryoserializer.buffer.max", "2047mb")
    .set("spark.driver.maxResultSize", "30g")
    .set("spark.kryo.referenceTracking", "false")
    .set("spark.executor.extraJavaOptions", "–XX:hashCode=0")
    .set("spark.driver.extraJavaOptions", "–XX:hashCode=0")
    .set("spark.executor.memory", "15g")
    //.set("spark.mongodb.input.uri", dbUrl+sourceDBName+"."+sourceCollectionName+"?retryWrites=true")
}
