name := "Search Indexer"
organization := "inviz.ai"
version := "0.1.0-SNAPSHOT"
scalaVersion := "2.12.8"
mainClass in Compile := Some("ai.inviz.Application")


lazy val root = (project in file("."))
  .settings(
    name := "search-indexer",
    idePackagePrefix := Some("ai.inviz")
  )

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.4",
  "org.apache.spark" %% "spark-sql" % "2.4.4",
  "com.typesafe" % "config" % "1.2.1",
  "com.zaxxer" % "HikariCP" % "2.4.5",
  "com.typesafe.slick" %% "slick" % "3.2.1",
  "com.typesafe.play" %% "play-json" % "2.6.13",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.8",
  "net.liftweb" %% "lift-json" % "3.4.0",
  "org.mongodb.spark" %% "mongo-spark-connector" % "2.4.2",
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.2",
  "org.elasticsearch" %% "elasticsearch-spark-20" % "7.16.2",
  "org.elasticsearch.client" % "elasticsearch-rest-client" % "6.1.1",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
)
